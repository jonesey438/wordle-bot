<?php

namespace App\Services;

use App\Models\Word;
use App\Models\Letter;

class WordleService
{
    private string $solution = '';
    private int $attempts = 0;
    private array $attemptedWords = [];
    private object $possibleWords;
    private object $possibleLetters;

    /**
     * @param string $solution
     * @param string $initialGuess
     * @return string|array
     *
     * Solves the Wordle
     */
    public function solve($solution, $initialGuess)
    {
        //Initialize variables
        $this->solution = $solution;
        $this->attempts = 0;
        $this->possibleWords = Word::all()->map(function ($word) {
            return $word->value;
        });
        $this->possibleLetters = Letter::all()->sortByDesc('frequency');
        $guess = $initialGuess;

        //Make sure the database contains the solution
        if (!$this->possibleWords->contains($solution)) {
            return 'The provided solution is not in the database.';
        }
        //Make sure the database contains the initial guess
        if (!$this->possibleWords->contains($initialGuess)) {
            return 'The provided initial guess is not in the database.';
        }

        while (true) {
            //Guess the word
            $this->guessWord($guess);
            array_push($this->attemptedWords, $guess);

            //Check to see if the word was narrowed down
            if ($this->possibleWords->count() == 1) {
                $remainingWord = $this->possibleWords->values()->first();
                //Account for case where there were 2 possible choices and the incorrect word was chosen
                if (!in_array($remainingWord, $this->attemptedWords)) {
                    array_push($this->attemptedWords, $remainingWord);
                    $this->attempts++;
                }
                //Formatting data to return to the user
                $attemptData = $this->formatWordAttemptsForReturn();

                //Return the number of attempts as well as the attemped words
                return $attemptData;
            }
            //If the word was not narrowed down, recalculate letter frequency and choose another word to guess.
            $this->updateLetterFrequency();
            $guess = $this->chooseNextWord();
        }
    }

    /**
     * @param string $solution
     * @param string $initialGuess
     * @return string|array
     *
     * Checks the provided word against the solution and narrows down the possible remaining choices based on how the
     * letters in the guess compare to the letters in the solution and their positions.
     */
    private function guessWord($guessWord)
    {
        //Check the initial word to see if the letter is in the word and in the right spot, in the word and in the wrong
        //spot, or not in the word at all.
        $currentGuessArray = collect(str_split($guessWord))
            ->map(function ($letter, $position) {
                if ($this->solution[$position] == $letter) {
                    return ['letter' => $letter, 'results' => 'correct'];
                }
                if (in_array($letter, str_split($this->solution))) {
                    return ['letter' => $letter, 'results' => 'wrong position'];
                }
                return ['letter' => $letter, 'results' => 'not in word'];
            })
            ->all();

        //Remove words from the remaining possible choices that contradict the data gathered on the current guess
        foreach ($currentGuessArray as $position => $letterData) {
            if ($letterData['results'] == 'correct') {
                //Remove words without the letter in that position
                $this->possibleWords = $this->possibleWords->filter(function ($word) use ($letterData, $position) {
                    if (str_split($word)[$position] == $letterData['letter'])
                        return $word;
                });
            }
            if ($letterData['results'] == 'wrong position') {
                //Remove words without the letter
                $this->possibleWords = $this->possibleWords->filter(function ($word) use ($letterData) {
                    if (str_contains($word, $letterData['letter'])) {
                        return $word;
                    }
                });
                //Remove words with the letter in that position
                $this->possibleWords = $this->possibleWords->filter(function ($word) use ($letterData, $position) {
                    if (!(str_split($word)[$position] == $letterData['letter']))
                        return $word;
                });
            }
            if ($letterData['results'] == 'not in word') {
                //Remove letter from possible letters
                $this->possibleLetters = $this->possibleLetters->filter(function ($letter) use ($letterData) {
                    if ($letter->value != $letterData['letter']) {
                        return $letter;
                    }
                });
                //Remove all words that contain this letter
                $this->possibleWords = $this->possibleWords->filter(function ($word) use ($letterData) {
                    if (!str_contains($word, $letterData['letter'])) {
                        return $word;
                    }
                });
            }
        }

        //Increment
        $this->attempts++;
    }

    /**
     * Update the letter frequency of all remaining possible words, removing any letters that are no longer used.
     */
    private function updateLetterFrequency()
    {
        $wordString = json_encode($this->possibleWords);
        $this->possibleLetters = $this->possibleLetters->each(function ($letter) use ($wordString) {
            $letterCount = substr_count($wordString, $letter->value);
            $letter->frequency = $letterCount;
        })->filter(function ($letter) {
            if ($letter->frequency != 0) {
                return $letter;
            }
        })
        ->sortByDesc('frequency');
    }

    /**
     * @return string $nextWord
     *
     * Pick the next word to guess.
     */
    private function chooseNextWord()
    {
        //Normalize the letter frequencies for the word score calculation
        $index = $this->possibleLetters->count();
        foreach ($this->possibleLetters as $letter) {
            $letter->frequency = $index;
            $index--;
        }

        //Loop through the possible words and assign each a score based on the normalized letter frequencies of all the
        //remaining words
        foreach ($this->possibleWords as $word) {
            $score = 0;
            foreach (str_split($word) as $letter) {
                $score += $this->possibleLetters->firstWhere('value', $letter)->frequency;
            }
            $wordScores[$word] = $score;
        }

        //Return the highest scoring word
        $nextWord = collect($wordScores)->sortDesc()->keys()->first();
        return $nextWord;
    }

    /**
     * @return array
     *
     * Make the returned data more pleasant to read.
     */
    private function formatWordAttemptsForReturn()
    {
        $formattedArray = [];
        $index = 1;

        //Add the attempt number to each guess
        foreach ($this->attemptedWords as $word) {
            array_push($formattedArray, "Attempt #" . $index . ": " . $word);
            $index++;
        }

        //Prepare the response message with the singular or plural attempt count
        $s = count($formattedArray) != 1 ? 's' : null;
        $responseString = "It took the bot " . count($formattedArray) . " attempt" . $s . " to solve the puzzle.";

        //Return the data
        return [$responseString, $formattedArray];
    }
}
