<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\WordleService;

class WordleBotController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * AI player that solves the Wordle by making calculated guesses until it narrows it down to the solution provided
     * in the request. The endpoint returns the number of attempts as well as the words attempted.
     */
    public function index(Request $request)
    {
        //Retrieve the daily solution from the request and hard code the initial guess
        $solution = strtolower($request->solution);
        $initialGuess = 'arose';

        //Solve the daily Wordle
        $service = new WordleService();
        $attemptData = $service->solve($solution, $initialGuess);

        //Return the attempt count along with the attempted words
        return response()->json($attemptData, 200);
    }
}
