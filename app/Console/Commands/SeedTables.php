<?php

namespace App\Console\Commands;

use App\Models\Word;
use App\Models\Letter;
use Illuminate\Console\Command;

class SeedTables extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seed:tables';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports the words and letters in App\Console\Commands\data along with letter frequency
        data to the words and letters tables.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return string
     */
    public function handle()
    {
        //Import words from file
        $wordContents = file_get_contents('data/words.txt', 'r');
        $wordData = explode(',\r\n', str_replace('"', "", substr(json_encode($wordContents), 0)));

        //Save each to the database
        foreach ($wordData as $word) {
            $wordModel = new Word();
            $wordModel->value = $word;
            $wordModel->save();
        }

        //Import letters from file and words from database
        $wordString = json_encode($wordData);
        $letterContents = file_get_contents('data/letters.txt', 'r');
        $letterCollection = collect(explode(',\r\n', str_replace('"', "", substr(json_encode($letterContents), 0))));

        //Set letter frequency of all words contained in the database
        $letterCollection->each(function ($letter) use ($wordString) {
            //Count number of times letter occurs in the database
            $letterCount = substr_count($wordString, $letter);

            //Create a new letter in the entry table with the character and frequency count
            Letter::create([
                'value' => $letter,
                'frequency' => $letterCount,
            ]);
        });

        return "Words and Letters tables successfully populated.";
    }
}
