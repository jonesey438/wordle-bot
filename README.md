## WordleBot:

A Wordle AI Player designed to compete alongside you. Enter the solution in the GET request and the bot will return the number of attempts it took to solve the puzzle and the words that it attempted.

![](example.png)

## Setup:
Run:

`composer install`

Copy .env from .env.example and run the following:

`php artisan migrate:refresh`

`php artisan seed:tables`

## APIs:

GET `/api/solve?solution=PUZZLESOLUTION`

-Replace PUZZLESOLUTION with the solution you would like the bot to attempt to figure out.